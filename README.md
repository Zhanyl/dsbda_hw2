# HOMEWORK 2 #

Application that calculate average salary and abroad trips count statistics about citizens. Row input format: passport �, month,  salary or passport �, month, number of abroad trips  

### REQUIREMENTS ###

Cloudera Quickstart VM 5.12.0, Apache Spark 2.1.0, 4+ GiB RAM

### SET UP ###

git clone  git@bitbucket.org:Zhanyl/dsbda_hw2.git 
cd dsbda_homework2 
mvn clean install

### INPUT DATA ###

 java -jar dsbda_hw2-1.0-SNAPSHOT-jar-with-dependencies.jar generate
chmod +x prepare_data.sh
./prepare_data.sh

### RUN ###
cd target
 java -jar dsbda_hw2-1.0-SNAPSHOT-jar-with-dependencies.jar processing
