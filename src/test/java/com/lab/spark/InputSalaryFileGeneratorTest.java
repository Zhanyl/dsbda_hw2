package com.lab.spark;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class InputSalaryFileGeneratorTest {

    InputSalaryFileGenerator generator = new InputSalaryFileGenerator();

    @Test
    public void generateInputFiles() {

        generator.generateInputFiles(1);
        File salary = new File("inputSalary.json");
        File trips = new File("inputTrips.json");

        assertEquals(true, salary.exists());
        assertEquals(true, trips.exists());

        salary.delete();
        trips.delete();
    }
}