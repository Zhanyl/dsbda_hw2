package com.lab.spark;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.lab.spark.data.PersonSalaryModel;
import com.lab.spark.data.PersonTripModel;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.Test;
import scala.Tuple2;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProcessingTest {

    @Test
    public void processingTest() {

          PairFunction<Tuple2<String, Tuple2<Integer, Integer>>,String,Integer> getAverageByKey = (tuple) -> {
            Tuple2<Integer, Integer> val = tuple._2;
            int total = val._1;
            int count = val._2;
            Tuple2<String, Integer> averagePair = new Tuple2<String, Integer>(tuple._1, total / count);
            return averagePair;
        };

        SparkConf conf = new SparkConf()
                .setAppName("Spark Big Data Processing")
                .setMaster("local");

        JavaSparkContext sc = new JavaSparkContext(conf);

        Configuration hadoopConf = new Configuration();
        hadoopConf.addResource(new Path("core-site.xml"));
        hadoopConf.addResource(new Path("hdfs-site.xml"));

        hadoopConf.set("fs.hdfs.impl",
                org.apache.hadoop.hdfs.DistributedFileSystem.class.getName()
        );
        hadoopConf.set("fs.file.impl",
                org.apache.hadoop.fs.LocalFileSystem.class.getName()
        );

        //inputSalaryList
        List<Tuple2<String,Integer>> inputSalaryList = new ArrayList<Tuple2<String,Integer>>();

        try {

            JsonReader readerSalary = new JsonReader(new FileReader("src/test/resources/inputSalary.json"));
            PersonSalaryModel[] salaryModels = new Gson().fromJson(readerSalary, PersonSalaryModel[].class);
            for (PersonSalaryModel salaryModel   : salaryModels) {
                inputSalaryList.add(new Tuple2<String, Integer>(salaryModel.getAgeCategory(), salaryModel.getSalary()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }




        //inputTripsList
        List<Tuple2<String,Integer>> inputTripsList = new ArrayList<Tuple2<String,Integer>>();

        try {

            JsonReader readerTrips = new JsonReader(new FileReader("src/test/resources/inputTrips.json"));
            PersonTripModel[] tripModels = new Gson().fromJson(readerTrips, PersonTripModel[].class);
            for (PersonTripModel tripsModel   : tripModels) {
               inputTripsList.add(new Tuple2<String,Integer>(tripsModel.getAgeCategory(), tripsModel.getAbroadTripsCount()));
            }
        } catch (IOException e) {
           e.printStackTrace();
        }
        System.out.println(inputSalaryList);
        System.out.println(inputTripsList);

        //parallelizePairsSalary
        JavaPairRDD<String, Integer> pairSalaryRDD = sc.parallelizePairs(inputSalaryList);
        //count each values per key
        JavaPairRDD <String, Tuple2<Integer, Integer>> valueCountSalary = pairSalaryRDD.mapValues(value -> new Tuple2<Integer, Integer>(value,1));
        //add values by reduceByKey
        JavaPairRDD<String, Tuple2<Integer, Integer>> reducedCountSalary = valueCountSalary.reduceByKey((tuple1,tuple2) ->  new Tuple2<Integer, Integer>(tuple1._1 + tuple2._1, tuple1._2 + tuple2._2));
        //calculate average
        JavaPairRDD<String, Integer> averagePairSalary = reducedCountSalary.mapToPair(getAverageByKey);


        //parallelizePairsTrips
        JavaPairRDD <String, Integer> pairTripsRDD = sc.parallelizePairs(inputTripsList);
        //count each values per key
        JavaPairRDD <String, Tuple2<Integer, Integer>> valueCountTrips = pairTripsRDD.mapValues(value -> new Tuple2<Integer, Integer>(value,1));
        //add values by reduceByKey
        JavaPairRDD<String, Tuple2<Integer, Integer>> reducedCountTrips = valueCountTrips.reduceByKey((tuple1,tuple2) ->  new Tuple2<Integer, Integer>(tuple1._1 + tuple2._1, tuple1._2 + tuple2._2));
        //calculate average
        JavaPairRDD<String, Integer> averagePairTrips = reducedCountTrips.mapToPair(getAverageByKey);

        //print averageByKey
//        averagePair.foreach(data -> {
//            System.out.println("Key="+data._1() + " Average=" + data._2());
//        });
        JavaPairRDD<String,Tuple2<Integer,Integer>> resultRdd = averagePairSalary.join(averagePairTrips);
        for(Tuple2<String,Tuple2<Integer,Integer>> tuple : resultRdd.collect())
            System.out.println(tuple._1()+":"+tuple._2()._1()+","+tuple._2()._2());

        for(Tuple2<String,Integer> tupleTrips : averagePairTrips.collect())
            System.out.println(tupleTrips._1()+":"+tupleTrips._2());

        for(Tuple2<String,Integer> tupleSalary : averagePairSalary.collect())
            System.out.println(tupleSalary._1()+":"+tupleSalary._2());


        //stop sc

        sc.stop();
        sc.close();

    }

}