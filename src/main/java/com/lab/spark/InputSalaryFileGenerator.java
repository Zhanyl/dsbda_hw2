package com.lab.spark;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.lab.spark.data.Month;
import com.lab.spark.data.PersonTripModel;
import com.lab.spark.data.PersonSalaryModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;



public class InputSalaryFileGenerator {

    private int minSalary = 50000;

    private int maxSalary = 5000000;

    private ObjectMapper objectMapper = new ObjectMapper();

    private Random random = new Random();

    List<PersonSalaryModel> personSalaryModelList = new ArrayList<>();
    List<PersonTripModel> personTripModelList = new ArrayList<>();

    /**
     *
     * ����� ��������� ������� ������. ��� ������������ ������ .json
     * @param count - ���������� �������, ���������� � ������� ����� �������� � ����
     *
     */
    public void generateInputFiles(int count) {

        PersonSalaryModel salaryModel;
        PersonTripModel tripModel;

        for (int i = 0; i < count; i++) {

            Integer passportNumber = 111111 + random.nextInt(888888);
            Integer age = 20 + random.nextInt(60);
            String ageCategory = defineAgeCategory(age);

            for (Month month: Month.values()) {

                salaryModel = new PersonSalaryModel();
                salaryModel.setPassportNumber(String.valueOf(passportNumber));
                salaryModel.setSalary(minSalary + random.nextInt(maxSalary - minSalary));
                salaryModel.setMonthNumber(String.valueOf(month.getNumber()));
                salaryModel.setAgeCategory(ageCategory);

                tripModel = new PersonTripModel();
                tripModel.setPassportNumber(String.valueOf(passportNumber));
                tripModel.setMonthNumber(String.valueOf(month.getNumber()));
                tripModel.setAbroadTripsCount(1 + random.nextInt(4));
                tripModel.setAgeCategory(ageCategory);

                personTripModelList.add(tripModel);
                personSalaryModelList.add(salaryModel);
            }
        }

        try {
            objectMapper.writeValue(new File("inputSalary.json"), personSalaryModelList);
            objectMapper.writeValue(new File("inputTrips.json"), personTripModelList);
        } catch (IOException e) {
            throw new RuntimeException("Error while generating file");
        }
    }

    /**
     *
     * ����� ������������ ����������� ������� �� ��������
     * @param age - �������
     * @return ���������� ���������: {young, middle, old}
     */
    private String defineAgeCategory(Integer age) {
        if(age >= 20 && age < 40) {
            return "young";
        } else if(age >= 40 && age < 60) {
            return "middle";
        } else if(age >= 60 && age <= 80) {
            return "old";
        } else {
            throw new RuntimeException("Error while defining age category");
        }
    }


}