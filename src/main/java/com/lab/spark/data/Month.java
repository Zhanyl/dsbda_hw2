package com.lab.spark.data;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
public enum Month {

    January(1),

    February(2),

    March(3),

    April(4),

    May(5),

    June(6),

    July(7),

    August(8),

    September(9),

    October(10),

    November(11),

    December(12);

    private int number;

    Month(int i) {
        this.number = i;
    }

    public List<Month> getAllMonths() {
        return Arrays.asList(Month.values());
    }
}